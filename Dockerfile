FROM python:3-alpine

LABEL io.k8s.name="Flask" \
      io.k8s.description="Lorem Ipsum Flask Application for Docker" \
      io.k8s.display-name="Lorem Ipsum" \
      io.k8s.version="0.1.0" \
      io.openshift.expose-services="8080:http" \
      io.openshift.tags="Lorem Ipsum,0.1.0,Flask"

ENV PORT=8080
WORKDIR /app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY . /app

EXPOSE ${PORT}

# TODO: Set the default CMD for the image
CMD gunicorn -b 0.0.0.0:${PORT} wsgi