import os
import sys


sys.path.insert(0, os.path.dirname(__file__))

from mygdr import create_app
application = create_app()

if __name__ == '__main__':
    # application.run(host="0.0.0.0", port=8080)
    application.run()
